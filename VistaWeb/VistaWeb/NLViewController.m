//
//  NLViewController.m
//  VistaWeb
//
//  Created by Jesús Alberto Ocaña Acosta on 12/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSString *direccion = @"http://www.criptidosdigitales.com";
    
    NSURL *url = [NSURL URLWithString:direccion];
    
    NSURLRequest *peticion = [NSURLRequest requestWithURL:url];
    
    [self.navegador loadRequest:peticion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
