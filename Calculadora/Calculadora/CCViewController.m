//
//  CCViewController.m
//  Calculadora
//
//  Created by Jesús Alberto Ocaña Acosta on 17/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import "CCViewController.h"

@interface CCViewController (){
    int contador;
    int longitud;
}
@end

@implementation CCViewController
@synthesize pantalla = _pantalla;
@synthesize primerNumero=_primerNumero;
@synthesize segundoNumero=_segundoNumero;
@synthesize operador=_operador;
@synthesize operadorAnterior=_operadorAnterior;
@synthesize res=_res;
@synthesize punteable=_punteable;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _pantalla.text=@"0";
    _primerNumero=0;
    _punteable=1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)presionarNumero:(id)sender {
    longitud=[_pantalla.text length];
    if (longitud<10 && _res==false) {
        
    if ([_pantalla.text isEqual:@"0"]) {
        _pantalla.text=@"";
    }
        
    NSString *numero = [sender currentTitle];
        if([numero isEqualToString:@"."]){
            if(_punteable==1){
                if([_pantalla isEqual:@""]){
                    _pantalla.text=@"0";
                }
                _pantalla.text=[_pantalla.text stringByAppendingString:numero];
                _punteable=0;
            }
            
        }else{
            _pantalla.text=[_pantalla.text stringByAppendingString:numero];
        }
    
        }
    
}


- (IBAction)resultado:(id)sender {
    float resultado = 0;
    _punteable=1;

    
    if([sender tag]==7){
    
        if(_res==false&&(_operador!=_operadorAnterior)){
            _segundoNumero=[_pantalla.text floatValue];
    switch (_operador) {
        case 2: _res=false;
            break;
        case 3:
            resultado =(_primerNumero/[_pantalla.text floatValue]);
           _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
            _res=true;
            break;
        case 4:
            resultado =_primerNumero*[_pantalla.text floatValue];
            _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
            _res=true;
            break;
        case 5:
            resultado =_primerNumero-[_pantalla.text floatValue];
           _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
            _res=true;
            break;
        case 6:
            resultado =_primerNumero+[_pantalla.text floatValue];
           _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
            _res=true;
            break;
        default:
            break;
                }
            _operadorAnterior=_operador;
            
    
        }else{
            _primerNumero=[_pantalla.text floatValue];
            switch (_operador) {
                
                case 3:
                    resultado =([_pantalla.text floatValue]/_segundoNumero);
                    _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
                    break;
                case 4:
                    resultado =[_pantalla.text floatValue]*_segundoNumero;
                    _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
                    break;
                case 5:
                    resultado =[_pantalla.text floatValue]-_segundoNumero;
                    _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
                    break;
                case 6:
                    resultado =[_pantalla.text floatValue]+_segundoNumero;
                    _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
                    break;
                default:
                    break;
            }
        
    }
        
    }

    }


- (IBAction)detectarTecla:(id)sender {
    _operador=[sender tag];
    _res=false;
    if(_operador==2){
        _primerNumero=0;
        _punteable=1;
    } else{
        _primerNumero = [_pantalla.text floatValue];
        _operadorAnterior= 0;
        _punteable=1;

    }
         _pantalla.text=@"0";
    
    
}

- (IBAction)cambioDeSigno:(id)sender {
    float resultado =[_pantalla.text floatValue]*(-1);
    _pantalla.text=[NSString stringWithFormat:@"%f", resultado];
    
}





@end
