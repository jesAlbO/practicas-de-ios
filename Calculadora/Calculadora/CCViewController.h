//
//  CCViewController.h
//  Calculadora
//
//  Created by Jesús Alberto Ocaña Acosta on 17/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *pantalla;
@property float primerNumero;
@property int operador;
@property float segundoNumero;
@property Boolean res;
@property int punteable;
@property int operadorAnterior;
- (IBAction)presionarNumero:(id)sender;
- (IBAction)resultado:(id)sender;
- (IBAction)detectarTecla:(id)sender;
- (IBAction)cambioDeSigno:(id)sender;


@end
