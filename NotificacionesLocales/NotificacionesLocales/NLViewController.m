//
//  NLViewController.m
//  NotificacionesLocales
//
//  Created by Jesús Alberto Ocaña Acosta on 05/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)notificar:(id)sender {
    NSDate *alertTime = [[NSDate date] dateByAddingTimeInterval:10];
    UIApplication *app = [UIApplication sharedApplication];
    UILocalNotification  *notifyAlarm = [[UILocalNotification alloc]init];
    
    if (notifyAlarm) {
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.timeZone =[NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        [notifyAlarm setApplicationIconBadgeNumber:1];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        notifyAlarm.soundName = @"Glass.aiff";
        notifyAlarm.alertBody = @"Esta es una notificación";
        
        [app scheduleLocalNotification:notifyAlarm];
    }
    
}
@end
