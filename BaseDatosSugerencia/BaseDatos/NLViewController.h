//
//  NLViewController.h
//  BaseDatos
//
//  Created by Jesús Alberto Ocaña Acosta on 17/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProxyBD.h"

@interface NLViewController : UIViewController
-(void)copiaBD;
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)oprimir:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (nonatomic, copy) NSString* selectedCellText;
@property (strong, nonatomic) ProxyBD *proxyBD;
- (IBAction)oprimirBorrar:(id)sender;
- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
