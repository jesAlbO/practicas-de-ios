//
//  CCViewController.m
//  ControlesComunes
//
//  Created by Jesús Alberto Ocaña Acosta on 17/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import "CCViewController.h"

@interface CCViewController ()



@end

@implementation CCViewController

@synthesize texto=_texto;
@synthesize etiqueta=_etiqueta;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidUnload{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)procesarInformacion:(id)sender {
    NSUInteger longitud=[_texto.text length];
    NSMutableString *rtr=[NSMutableString stringWithCapacity:longitud];
    while(longitud>(NSUInteger)0){
        unichar uch=[_texto.text characterAtIndex:--longitud];
        [rtr appendString:[NSString stringWithCharacters:&uch length:1]];
    }
    
    _etiqueta.text=rtr;
     rtr=_texto.text;
}

- (IBAction)terminaEdicion:(id)sender {
    [_texto resignFirstResponder];
}
@end
