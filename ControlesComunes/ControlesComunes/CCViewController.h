//
//  CCViewController.h
//  ControlesComunes
//
//  Created by Jesús Alberto Ocaña Acosta on 17/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
- (IBAction)procesarInformacion:(id)sender;
- (IBAction)terminaEdicion:(id)sender;



@end
