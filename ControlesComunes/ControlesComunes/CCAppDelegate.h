//
//  CCAppDelegate.h
//  ControlesComunes
//
//  Created by Jesús Alberto Ocaña Acosta on 17/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
