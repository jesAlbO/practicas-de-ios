//
//  MOAppDelegate.h
//  EjemploModal
//
//  Created by Jesús Alberto Ocaña Acosta on 05/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
