//
//  MODetalleViewController.h
//  EjemploModal
//
//  Created by Jesús Alberto Ocaña Acosta on 05/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MODetalleViewController;

@protocol AlumnoDetallesViewControllerDelegate <NSObject>

-(void)AlumnoDetallesViewControllerDelegateDidSave:(MODetalleViewController *)controller;
-(void)AlumnoDetallesViewControllerDelegateDidCancel:(MODetalleViewController *)controller;

@end

@interface MODetalleViewController : UITableViewController

@property (nonatomic, weak)id <AlumnoDetallesViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *nombre;


@end





