//
//  MOTableViewControllerAlumnosViewController.h
//  EjemploModal
//
//  Created by Jesús Alberto Ocaña Acosta on 05/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MODetalleViewController.h"

@interface MOTableViewControllerAlumnos : UITableViewController
<AlumnoDetallesViewControllerDelegate>

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end
