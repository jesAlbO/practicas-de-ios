//
//  main.m
//  ControlesAvanzados
//
//  Created by Jesús Alberto Ocaña Acosta on 21/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAAppDelegate class]));
    }
}
