//
//  CAAppDelegate.h
//  ControlesAvanzados
//
//  Created by Jesús Alberto Ocaña Acosta on 21/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
