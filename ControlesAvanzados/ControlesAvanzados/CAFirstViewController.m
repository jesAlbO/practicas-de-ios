//
//  CAFirstViewController.m
//  ControlesAvanzados
//
//  Created by Jesús Alberto Ocaña Acosta on 21/02/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import "CAFirstViewController.h"

@interface CAFirstViewController ()

    


@end

@implementation CAFirstViewController

NSArray *unidades;
int tipoDeUnidades=0;

- (void)viewDidLoad
{
   [super viewDidLoad];
    unidades = [NSArray arrayWithObjects:
               [NSArray arrayWithObjects:@"Longitud",@"centimetros", @"metro", @"kilometro",@"pie", nil],
               [NSArray arrayWithObjects:@"Àrea",@"hectareas", @"metros cuadrados", nil],
               [NSArray arrayWithObjects:@"Volumen", @"litros", @"metros cubicos", nil], nil
               ];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidUnload{
    unidades=nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark pickerView

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0)
        return [unidades count];
    
    return [[unidades objectAtIndex:tipoDeUnidades]count]-1;
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component == 0)
        if(tipoDeUnidades != row){
            tipoDeUnidades = row;
            [pickerView reloadComponent:1];
            
        }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(component == 0)
        return [[unidades objectAtIndex:row]objectAtIndex:0];
    return [[unidades objectAtIndex:tipoDeUnidades]objectAtIndex:row+1];
}



@end
