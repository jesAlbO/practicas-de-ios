//
//  CACiudadesViewController.h
//  ControlesAvanzados
//
//  Created by Jesús Alberto Ocaña Acosta on 05/03/14.
//  Copyright (c) 2014 Jesús Alberto Ocaña Acosta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CACiudadesViewController : UITableViewController

@property NSArray *ciudades;

@end
