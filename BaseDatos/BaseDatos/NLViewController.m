//
//  NLViewController.m
//  BaseDatos
//
//  Created by Jesús Alberto Ocaña Acosta on 17/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController
@synthesize proxyBD;

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self copiaBD];
    self.proxyBD=[[ProxyBD alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)copiaBD{
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *bd = [documentsPath stringByAppendingPathComponent:@"mibd"];
    BOOL existeArchivo = [[NSFileManager defaultManager]fileExistsAtPath:bd];
    
    if(existeArchivo)return;
    
    NSString *rutaResource = [[[NSBundle mainBundle]resourcePath]
                              
                              stringByAppendingPathComponent:@"mibd"];
    NSFileManager *administradorArchivos = [NSFileManager defaultManager];
    
    NSError *error = nil;
    
    if (![administradorArchivos copyItemAtPath:rutaResource toPath:bd error:&error]) {
        UIAlertView *mensajeError = [[UIAlertView alloc] initWithTitle:@"¡Cuidado!" message:@"No pude copiar la BD" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [mensajeError show];
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.proxyBD nombres]count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [[self.proxyBD nombres]objectAtIndex:indexPath.row];
    return cell;
}

- (IBAction)oprimir:(id)sender {
    [self.texto resignFirstResponder];
    [self.proxyBD insertarNombre:self.texto.text];
    [self.tabla reloadData];
}
@end
