//
//  ProxyBD.h
//  BaseDatos
//
//  Created by Jesús Alberto Ocaña Acosta on 17/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProxyBD : NSObject

-(NSMutableArray *)nombres;
-(void)insertarNombre:(NSString *)nombre;

@end
