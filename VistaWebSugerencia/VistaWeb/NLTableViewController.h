//
//  NLTableViewController.h
//  VistaWeb
//
//  Created by Jesús Alberto Ocaña Acosta on 28/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLTableViewController : UITableViewController
@property (nonatomic, copy) NSString* selectedCellText;



@end
