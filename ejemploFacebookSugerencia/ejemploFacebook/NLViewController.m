//
//  NLViewController.m
//  ejemploFacebook
//
//  Created by Jesús Alberto Ocaña Acosta on 12/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import "NLViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface NLViewController ()

@end

@implementation NLViewController
@synthesize texto = _texto;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)oprimir:(id)sender {
    SLComposeViewController *controladorSocial;
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        
    {
        controladorSocial = [[SLComposeViewController alloc]init];
        
        controladorSocial =[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controladorSocial setInitialText:self.texto.text];
        [self presentViewController:controladorSocial animated:YES completion:nil];
         }
         [controladorSocial setCompletionHandler:^(SLComposeViewControllerResult result)
          {
          
          NSString *output;
          switch(result){
              case SLComposeViewControllerResultCancelled:
                  output = @"Cancelado";
                  break;
                  
              case SLComposeViewControllerResultDone:
                  output = @"Trivia social posteada";
                  break;
              default:
                  break;
          }
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
          }];
}
@end
