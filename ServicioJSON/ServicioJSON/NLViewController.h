//
//  NLViewController.h
//  ServicioJSON
//
//  Created by Jesús Alberto Ocaña Acosta on 17/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)escribirCodigo:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *temperatura;
@property (strong, nonatomic) IBOutlet UILabel *presion;
@property (strong, nonatomic) IBOutlet UILabel *poblacion;
-(void)recogerDatos:(NSData *)responseData;

@end
