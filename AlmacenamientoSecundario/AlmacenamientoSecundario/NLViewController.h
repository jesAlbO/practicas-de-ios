//
//  NLViewController.h
//  AlmacenamientoSecundario
//
//  Created by Jesús Alberto Ocaña Acosta on 06/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
-(NSString *)leerDatos;

@end
