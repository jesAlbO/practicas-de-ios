//
//  NLViewController.m
//  AlmacenamientoSecundario
//
//  Created by Jesús Alberto Ocaña Acosta on 06/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

-(NSString *)leerDatos{
    NSString *directorio=[NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *rutaCompleta=[directorio stringByAppendingPathComponent:@"archivo"];
    
    NSStringEncoding codificacion = NSUTF8StringEncoding;
    NSError *error;
    
    NSString *textoLeido=[NSString stringWithContentsOfFile:rutaCompleta encoding:codificacion error:&error];
    
    if(textoLeido==nil) textoLeido=@"no inicializado";
    
    return textoLeido;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.texto.text=[self leerDatos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
