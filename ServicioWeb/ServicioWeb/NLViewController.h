//
//  NLViewController.h
//  ServicioWeb
//
//  Created by Jesús Alberto Ocaña Acosta on 13/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController <NSXMLParserDelegate>
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)terminarEscribir:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
@property (strong, nonatomic) NSMutableData *datosWeb;

@end
