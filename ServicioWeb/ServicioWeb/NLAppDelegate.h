//
//  NLAppDelegate.h
//  ServicioWeb
//
//  Created by Jesús Alberto Ocaña Acosta on 13/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
