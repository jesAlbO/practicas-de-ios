//
//  NLViewController.h
//  ejemploFacebook
//
//  Created by Jesús Alberto Ocaña Acosta on 12/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
- (IBAction)oprimir:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *texto;

@end
