//
//  NLViewController.h
//  NotificacionesLocales
//
//  Created by Jesús Alberto Ocaña Acosta on 05/03/14.
//  Copyright (c) 2014 ITLM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
- (IBAction)notificar:(id)sender;
- (IBAction)notificarB:(id)sender;

@end
